#lang info
(define collection "ogion")
(define deps '("base" "bencode-codec"))
(define build-deps '("scribble-lib" "racket-doc"))
(define scribblings '(("scribblings/ogion.scrbl" ())))
(define pkg-desc "An nREPL server for Racket.")
(define version "0.1.0")
(define pkg-authors '("technomancy@gmail.com"))
